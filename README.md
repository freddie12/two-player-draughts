# README #

Two player Draughts game made written in Java. Using Sockets to communicate game status through a server to the clients. 

###How To Play###

Batch files have been made for simplicity. 

Server.bat and client.bat can be started in any order.

However, compile.bat must be ran FIRST!



###If you would like to compile the java files for yourself via command line then please do the following:###

* Navigate to the root directory /Game.

* javac Draughts/src/java/com/draughts/common/\*.java Draughts/src/java/com/draughts/client/\*.java Draughts/src/java/com/draughts/server/*.java -d out

* cd out

* java com/draughts/server/Server

***open new terminal window***

* java com/draughts/client/Client

***open new terminal window***

* java com/draughts/client/Client


###Java Version###
The java version used was 1.7