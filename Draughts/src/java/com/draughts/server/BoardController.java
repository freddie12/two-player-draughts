package com.draughts.server;

import com.draughts.common.Board;
import com.draughts.common.Updatable;

/**
 * Controls the board objects variables.
 */
public class BoardController implements Updatable {

    private Board board;

    public BoardController(Board board) {
        this.board = board;
    }

    /*
     * Mutators
     * ----------------------------------------------------------------------
     */

    /** Save the index of the selected piece to be moved */
    public void setMoveOrigin(int[] index) { board.setMoveOrigin(index); }
    /** Set the piece index that needs to be removed */
    public void setRemovePiece(int[] index) { board.setRemovePiece(index); }
    /** Set move destination */
    public void setMoveDestination(int[] index) { board.setMoveDestination(index); }
    /** Set the destinations value. */
    public void setMoveDestinationValue(int value) { board.setMoveDestinationVal(value); }

    /*
     * Getters
     * -----------------------------------------------------------------------
     */

    /** Return the move origin */
    public int[] getMoveOrigin() { return board.getMoveOrigin(); }
    /** Return the move destination */
    public int[] getMoveDestination() { return board.getMoveDestination(); }
    /** Return the remove piece */
    public int[] getRemovePiece() { return board.getRemovePiece(); }
    /** Return the map of all the pieces */
    public int[][] getPieceMap() { return board.getPieceMap(); }
    /** Return the value of the destination */
    public int getMoveDestinationValue() { return board.getMoveDestinationValue(); }


    /*
     * Methods
     * -----------------------------------------------------------------------
     */

    /** Make the specified index a king value */
    public void makeKing(int[] index, int pieceValue) {
        board.setSquareStatus(index, pieceValue + pieceValue);
    }
    /** The the pieces origin to destination */
    public boolean move(int[] destination) {

        board.setSquareStatus(destination, board.getMoveDestinationValue());
        board.setSquareStatus(board.getMoveOrigin(), 0);
        if(board.getRemovePiece()[0] != -1)
            removePiece(board.getRemovePiece());

        return true;

    }
    /** Removes a piece specified in the given argument from the board */
    public void removePiece(int[] index) {
        board.setSquareStatus(index, 0);
    }

    /** Debuggin */
    public void printBoard() { board.printBoard(); }
}
