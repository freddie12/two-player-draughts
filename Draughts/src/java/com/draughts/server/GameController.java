package com.draughts.server;

import com.draughts.common.Constants;

/**
 * This class is to control the flow of the game. It recieves the moves from the users which have already been checked for move validity
 * locally. This simply runs the game till there is a winner or connection loss.
 */
public class GameController implements Runnable {

    // The game instance to control
    private Game game;

    // Control the board
    BoardController boardController;

    // Constructor
    public GameController(Game game) {
        // Reference a new Game
        this.game = game;
        // Wait for the board to callback to this
        boardController = new BoardController(game.getBoard());
        // Set the game flag
        game.setAlive(true);
    }

    /** Continue to run while the game doesn't have a winner or receive an exception detailing an connection error. */
    public void run() {
        String line;
        // Make both players proceed to the game loop
        game.getP1Out().println("continue");
        game.getP2Out().println("continue");
        // Wait for the first players move
        try {
            while (game.isAlive()) {
                // Read the players turn
                readMove();
                // Save the move
                boardController.move(boardController.getMoveDestination());
                // Send the players move
                sendMove();
                // Check if a player has won
                if(hasWon()) {
                    // Set the winner flag
                    game.setWinner(game.getTurn());
                    // Notify the players
                    notifyGameEnd();
                    // Stop the game
                    game.setAlive(false);
                    return;
                }

                if (readMessage().equals(Constants.MOVE_COMPLETE))
                    // Switch player only if the message says so else the player has another go
                    switchTurn();
                else {
                    moveIncomplete();
                }
            }

        } catch(Exception e) {
            game.setAlive(false);
            connectionLost();
        }

    }

    /*
     * Methods
     * -----------------------------------------------------------------------
     */

    /** Connection lost from one of the clients. */
    public void connectionLost() {
        try {
            game.getP1Out().println(Constants.CONNECTION_LOSS);
            game.getP2Out().println(Constants.CONNECTION_LOSS);
        } catch(NullPointerException ioe) { System.out.println("Tried to send connection lost") ;}
    }

    /** Move incomplete so send blank message in order to keep the same turn */
    public void moveIncomplete() {
        if(game.getTurn() == 1)
            game.getP2Out().println("");
        else
            game.getP1Out().println("");
    }
    /** Notify the players the game has ended. */
    public void notifyGameEnd() {
        game.getP1Out().println(Constants.GAME_OVER);
        game.getP1Out().println(game.getTurn());
        game.getP2Out().println(Constants.GAME_OVER);
        game.getP2Out().println(game.getTurn());
    }
    /** Send the move based on the whos turn it is */
    public void sendMove() {
        // Send the origin index, destination index, destination value, remove index
        if(game.getTurn() == 1) {
            game.getP2Out().println(arrayToString(boardController.getMoveOrigin()));
            game.getP2Out().println(arrayToString(boardController.getMoveDestination()));
            game.getP2Out().println(boardController.getMoveDestinationValue());
            game.getP2Out().println(arrayToString(boardController.getRemovePiece()));
        } else {
            game.getP1Out().println(arrayToString(boardController.getMoveOrigin()));
            game.getP1Out().println(arrayToString(boardController.getMoveDestination()));
            game.getP1Out().println(boardController.getMoveDestinationValue());
            game.getP1Out().println(arrayToString(boardController.getRemovePiece()));
        }
    }
    /** Read in the move vars depending on whos turn it is. Store them inside the board. */
    public String readMove() throws Exception {
        String line;

            // Origin index
            line = readMessage();
            boardController.setMoveOrigin(stringToArray(line));
            // Destination index
            line = readMessage();
            boardController.setMoveDestination(stringToArray(line));
            // Destination value
            line = readMessage();
            boardController.setMoveDestinationValue(Integer.parseInt(line));
            // Remove piece value
            line = readMessage();
            boardController.setRemovePiece(stringToArray(line));

        return line;
    }
    /** Read a message containing the constants following protocol */
    public String readMessage() throws Exception {
        String line = "";

            if(game.getTurn() == 1)
                line = game.getP1In().readLine();
            else
                line = game.getP2In().readLine();
        return line;
    }
    /** Return true if any pieces of the opposite type are found */
    public boolean hasWon() {
        int opposingPieceId = game.getTurn() * -1;
        for (int[] y : boardController.getPieceMap())
            for (int x : y)
                if (x == opposingPieceId || x == opposingPieceId + opposingPieceId)
                    return false;
        return true;
    }
    /** Takes an index e.g. line = "2,5" and converts to int[] array. e.g. int[] {2, 5}; */
    private int[] stringToArray(String line) {
        String[] tempStr = line.split(",");
        return new int[] {Integer.parseInt(tempStr[0]), Integer.parseInt(tempStr[1])};
    }
    /** Takes an int[2][6] and converts it to a readable string "2,6" */
    public String arrayToString(int[] index) {
        String s = index[0] + "," + index[1];
        return s;
    }
    /** Switch the turn and let the players know. */
    public void switchTurn() {
        if(game.getTurn() == 1) {
            game.getP2Out().println(Constants.MOVE_COMPLETE);
        } else {
            game.getP1Out().println(Constants.MOVE_COMPLETE);
        }
        game.switchTurn();
    }

}