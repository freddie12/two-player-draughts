package com.draughts.server;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;

/**
 * This class waits for users to connect to the socket through the defined port.
 * If a user connects we wait for the 2nd user to connect, however, before we connect we check
 * if there is already a connected user. If there is then that means we can start a new game with two users.
 * If the player, however, quits the connection after joining then the 2nd player becomes the only player in the array
 * and therefore has to wait for a 2nd player.
 *
 * Once a game is started on a different thread we continue to wait for users to join continually creating 2 player games.
 *
 * @see Socket
 * @see com.draughts.server.GameController
 * @see com.draughts.server.Game
 */
public class Server {

    // The writers to send messages to
    ArrayList<PrintWriter> writers;

    // The users sockets
    ArrayList<Socket> sockets;


    public Server() {

        try {

            ServerSocket sock = new ServerSocket(9000);

            System.out.println("Server started @ port:9000");



            while (true) {

                // Start with 0 players
                socketReset();
                System.out.println("Waiting for players to join to start a fresh game...");


                // Until two players have joined
                while(writers.size() < 2) {

                    Socket tempSock = sock.accept();

                    // Check if the socket is still open otherwise remove it
                    if(writers.size() == 1) {

                        if (checkClosed()) {
                            System.out.println("Player 1 left");
                            socketReset();
                        }
                    }

                    // Save the socket
                    sockets.add(tempSock);

                    // Save the writer
                    writers.add(new PrintWriter(tempSock.getOutputStream()));

                    // Decide the id
                    String id = (writers.size()<2 ? "1" : "-1");

                    // Give the player the id
                    writers.get(writers.size() - 1).println(id);
                    writers.get(writers.size() - 1).flush();
                    System.out.println("Player " + writers.size() + " has joined");

                }

                System.out.println("Starting new game..");

                // Start a new thread so other games can start simultaniously
                new Thread(new GameController(new Game(sockets.get(0), sockets.get(1)))).start();

            }

        } catch(SocketException so) {
            System.out.println("Socket failure");
        }
        catch (IOException ioe) {
            System.out.println("Failure getting socket output stream");
        }

    }

    /**
     * Empty the @see ArrayList
     */
    public void socketReset() {
        sockets = new ArrayList<Socket>();
        writers = new ArrayList<PrintWriter>();
    }

    /**
     * Write to the writers to check if errors occur. Return true if the errors occur when socket connection has been lost.
     * @return
     */
    public boolean checkClosed() {

        // Need to write twice before an error occurs ???
        writers.get(0).println(1);
        writers.get(0).flush();
        writers.get(0).println(1);
        writers.get(0).flush();

        boolean m = writers.get(0).checkError();
        return m;

    }


    public static void main(String[] args) {

        new Server();

    }

}