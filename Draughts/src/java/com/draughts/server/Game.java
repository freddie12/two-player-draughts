package com.draughts.server;

import com.draughts.common.Board;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Game {

    // Players
    private BufferedReader p1In;
    private PrintWriter    p1Out;
    private BufferedReader p2In;
    private PrintWriter    p2Out;

    // Game loop flag
    private boolean alive;

    // Game winner
    private int winner = 0;

    // The turn
    private int turn = 1;

    // Game status

    // The board
    private Board board;

    // Constructor
    public Game(Socket one, Socket two) {

        // Init the board
        board = new Board();

        try {
            p1In  = new BufferedReader(new InputStreamReader(one.getInputStream()));
            p1Out = new PrintWriter(one.getOutputStream(), true);
            p2In  = new BufferedReader(new InputStreamReader(two.getInputStream()));
            p2Out = new PrintWriter(two.getOutputStream(), true);

        } catch(IOException ioe) { ioe.printStackTrace(); }

    }

    /*
     * Mutators
     * ----------------------------------------------------------------------
     */

    public int switchTurn() {
        if (turn == 1) turn = -1;
        else turn = 1;

        return turn;
    }
    public void setAlive(boolean alive) { this.alive = alive; }
    public void setWinner(int winner) { this.winner = winner; }

    /*
     * Getters
     * -----------------------------------------------------------------------
     */

    public BufferedReader getP1In() { return p1In; }
    public PrintWriter getP1Out() { return p1Out; }
    public BufferedReader getP2In() { return p2In; }
    public PrintWriter getP2Out() { return p2Out; }
    public int getTurn() {
        return turn;
    }
    public Board getBoard() {
        return board;
    }
    public boolean isAlive() { return alive; }

}
