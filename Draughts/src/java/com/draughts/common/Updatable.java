package com.draughts.common;

/**
 * Implementation of this to update the game
 */
public interface Updatable {

    public boolean move(int[] destination);

    public void setMoveOrigin(int[] index);

    public void removePiece(int[] index);

    public void makeKing(int[] index, int pieceValue);

}
