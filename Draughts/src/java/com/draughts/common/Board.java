package com.draughts.common;

/**
 * Square Draughts or Chess board. Holds the status of the playing board.
 * Each square has a value which details a particular player or empty.
 * This board is controlled by a instance of BoardController
 * @see com.draughts.client.BoardController
 */
public class Board {

    // Board dimension
    public static final int BOARD_SIZE = 8;

    // Selected
    private int[] moveOrigin = {-1, -1};
    private int[] moveDestination = {-1, -1};
    private int moveDestinationVal;

    // The removed piece index will be allocated to here.
    private int[] removePiece = {-1, -1};

    // Hold the status of each square
    private int[][] pieceMap = {
            {0, -1, 0, -1, 0, -1, 0, -1},
            {-1, 0, -1, 0, -1, 0, -1, 0},
            {0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0},
            {0, 1, 0, 1, 0, 1, 0, 1},
            {1, 0, 1, 0, 1, 0, 1, 0}
    };

    // Holds the map for the squares
    private int[][] squareMap = {
            {0, 1, 0, 1, 0, 1, 0, 1},
            {1, 0, 1, 0, 1, 0, 1, 0},
            {0, 1, 0, 1, 0, 1, 0, 1},
            {1, 0, 1, 0, 1, 0, 1, 0},
            {0, 1, 0, 1, 0, 1, 0, 1},
            {1, 0, 1, 0, 1, 0, 1, 0},
            {0, 1, 0, 1, 0, 1, 0, 1},
            {1, 0, 1, 0, 1, 0, 1, 0}
    };

    /*
     * Mutators
     * ----------------------------------------------------------------------
     */
    /** Sets the origin of a move as a int array. The array is the index of the square map. */
    public void setMoveOrigin(int[] moveOrigin) {
        this.moveOrigin = moveOrigin;
    }
    /** Sets the destination of the move as an int array. The array is the index of the square map. */
    public void setMoveDestination(int[] moveDestination) {
        this.moveDestination = moveDestination;
    }

    public void setMoveDestinationVal(int moveDestinationVal) { this.moveDestinationVal = moveDestinationVal; }

    /** Sets the status as an integer of a specified map index. */
    public void setSquareStatus(int[] index, int value) {
        pieceMap[index[0]][index[1]] = value;
    }
    /** Resets the move variables to -1, -1. This shows that no move variables have been set. */
    public void resetMoveVariables() {
        int[] i = {-1, -1};
        moveOrigin = moveDestination = i;
    }
    /** Sets an index of a piece in the map that should be removed in a move. */
    public void setRemovePiece(int[] index) {
        this.removePiece = index;
    }

    /*
     * Getters
     * -----------------------------------------------------------------------
     */
    /** Returns the map of pieces in a 2d int array */
    public int[][] getPieceMap() { return pieceMap; }
    /** Returns the map of the board in a 2d int array */
    public int[][] getSquareMap() { return squareMap;}
    /** Returns the status of the square for the given index. e.g. -1/1 is normal piece. -2/2 is a king. 0 is an empty square */
    public int getSquareStatus(int[] index) {
        return pieceMap[index[0]][index[1]];
    }
    /** Return the square index of the users specified move origin */
    public int[] getMoveOrigin() {
        return moveOrigin;
    }
    /** Returns true if the move origin has been set */
    public boolean moveOriginIsSet() {
        if (moveOrigin[0] != -1)
            return true;
        return false;
    }
    /** Returns the moveDestination */
    public int[] getMoveDestination() {
        return moveDestination;
    }
    public int getMoveDestinationValue() { return moveDestinationVal; }
    /** Returns the value of the given square index */
    public int getSquareMapSquareValue(int[] index) {
        return squareMap[index[0]][index[1]];
    }
    /** Returns the index of the remove piece*/
    public int[] getRemovePiece() {
        return removePiece;
    }


    /*
     * Methods
     * -----------------------------------------------------------------------
     */
    /** Returns true if square index is empty. */
    public boolean empty(int[] index) {

        // Make sure it is in the array bounds
        if (!(index[0] <= -1) && !(index[1] <= -1) && !(index[0] >= BOARD_SIZE) && !(index[1] >= BOARD_SIZE))
            return (pieceMap[index[0]][index[1]] == 0);

        return false;

    }

    /** Print the array of pieces. For debugging */
    public void printBoard() {

        for (int[] a : pieceMap) {
            // New line
            System.out.println("");
            for (int b : a)
                System.out.print(b + "\t");
        }
        System.out.println(" ");
        System.out.println(" ");

    }

}