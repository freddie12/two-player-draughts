package com.draughts.common;

/**
 * Used as a means of communication between @see Server and @see DraughsClient
 */
public interface Constants {
    public static final String GAME_OVER = "gameover";
    public static final String MOVE_COMPLETE = "movecomplete";
    public static final String USER_LEFT = "userleft";
    public static final String CONNECTION_LOSS = "connectionloss";
}