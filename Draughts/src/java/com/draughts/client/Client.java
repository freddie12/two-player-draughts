package com.draughts.client;

import java.io.IOException;
import java.net.Socket;

/**
 *
 * Initially a menu display will be shown. Once the frame listener gets notification that the user wants to start a new game
 * startNewGame() will be called. If the socket is already available then reconnect to the Server ultimately starting as fresh
 * player. Dispose of the current JFrame and open up a new GameBoard. The game will start in a new thread in order for the user
 * to be able to still interact with the board while the game listens for reads from the server.
 * @see GameController
 * @see GameFrame
 * @see MenuFrameDisplay
 * @see Thread
 *
 */
public class Client {

    // The game controller
    GameController game;
    // JFrame to hold the display
    GameFrame frame;
    // The display that is given to the GameFrame
    MenuFrameDisplay display;
    // Socket to connect to the server
    Socket sock;

    /*
     * Construct
     * ----------------------------------------------------------------------
     */
    public Client() {
        display = new MenuFrameDisplay();
        frame = new GameFrame(this, "Draughts", display);
    }

    /*
     * Methods
     * ----------------------------------------------------------------------
     */
    /** Start a new game refreshing the connection to the server. If a connection is already present then close it. */
    public void startNewGame() {
        try {
            // Close the socket if one already open
            if (sock != null)
                sock.close();
            // Connect to the server again
            sock = new Socket("localhost", 9000);
            // Remove current window
            frame.dispose();
            // Start a new game with the socket
            game = new GameController(new Game(), sock);
            // Start this on a new thread so the display can be viewed in the current thread.
            (new Thread(game)).start();
            // Start a new display for the game
            frame = new GameFrame(this, "Draughts", game.getDisplay());
        } catch (IOException ioexception) {
            // No connection can be made to the server
            frame.getDisplayable().setMessage("Server down... Please try again later.");
        }
    }

    /** Closes the current socket connection. May be called when the user decides to start a new game. */
    public void close() {
        try {
            if (sock != null)
                sock.close();
        } catch (IOException ioexception) {
            ioexception.printStackTrace();
        }
    }

    /** Start the programme. */
    public static void main(String args[]) {
        new Client();
    }

}