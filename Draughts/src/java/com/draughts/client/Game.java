package com.draughts.client;

import com.draughts.common.Board;

/**
 * Contains all of  the games variables. Is manipulated by a GameController
 * @see GameController
 * */
public class Game {

    // The board object
    private Board board;
    // The turn it is to move. Starts with player 1
    private int     turn = 1;
    // The players id. Receives it from the server
    private int     playerId;
    // Gets set when the game starts
    private boolean isAlive = false;

    /*
     * Construct
     * ----------------------------------------------------------------------
     */
    public Game() { /* empty construct */ }

    /*
     * Mutators
     * ----------------------------------------------------------------------
     */
    public void setIsAlive(boolean flag) { isAlive = flag; }
    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }
    public void setTurn(int turn) { this.turn  = turn; }

    /*
     * Getters
     * ----------------------------------------------------------------------
     */
    public int getPlayerId() {
        return playerId;
    }
    public Board getNewBoard() {
        return board = new Board();
    }
    public int getTurn() {
        return turn;
    }
    public Board getBoard() {
        return board;
    }
    public boolean isAlive() { return isAlive; }

    /*
     * Methods
     * ----------------------------------------------------------------------
     */
    // Switch from 1 to -1
    public int switchTurn() { return turn = (turn == 1 ? -1 : 1); }

}