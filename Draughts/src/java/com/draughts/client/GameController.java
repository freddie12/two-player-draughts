package com.draughts.client;

import java.io.*;
import java.net.Socket;

/**
 * Controls the GameStatus flow on a separate thread.
 * Reads/Writes to the DraughtServer until the game play ends dictated by the server.
 */
public class GameController implements Runnable {

    private Game            game;
    private BoardController boardController;
    private Socket          sock;
    private BufferedReader  serverIn;
    private PrintWriter     serverOut;

    /*
     * Construct
     * ----------------------------------------------------------------------
     */
    /**
     * Set up the BoardController, Socket in/out
     * @param game Receive a Game to control
     * @param socket Receive a Socket connected to the Server
     */
    public GameController(Game game, Socket socket) {
        this.game = game;
        boardController = new BoardController(this, game.getNewBoard());
        boardController.setDisplay();
        sock = socket;
        try {
            serverIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            serverOut = new PrintWriter(socket.getOutputStream(), true);
        } catch (IOException ioexception) {
            ioexception.printStackTrace();
        }
    }

    /*
     * Getters
     * ----------------------------------------------------------------------
     */
    /** Return a JPanel. */
    public Display getDisplay() {
        return boardController.getDisplay();
    }
    /** Return whether the game is alive or not. */
    public boolean getIsAlive() {
        return game.isAlive();
    }
    /** Return what player should move next. */
    public int getTurn() {
        return game.getTurn();
    }
    /** Return the players id. */
    public int getPlayerId() {
        return game.getPlayerId();
    }

    /*
     * Methods
     * ----------------------------------------------------------------------
     */
    /** Invoked from the Thread object and continues until the server ends the game loop. */
    public void run() {
        String line = "";

        try {

            // Recieve the id of your player. Either 1 or -1
            game.setPlayerId(Integer.parseInt(readMessage()));
            System.out.println("Player id: " + game.getPlayerId());

            // Update the display with message
            boardController.updateGameStatus("Waiting for opponent");
            System.out.println("Waiting for opponent to join");

            // Empty just to make the player wait till another player has arrived
            readMessage();

            // Continue to loop
            game.setIsAlive(true);

            // Set the appropriate message in the display
            if (game.getPlayerId() == game.getTurn())
                boardController.updateGameStatus("Make your move");
            else
                boardController.updateGameStatus("Waiting for opponent move");

            // Loop until the game isn't alive
            while (game.isAlive()) {

                // The first message from the server. connection loss, gameover or 1,2 stating the first move origin
                line = readMessage();

                // Break out of the loop immedietly
                if (line.equals("connectionloss") || line.equals("gameover"))
                    break;

                // Make sure that it's not this players turn in order to read the opponents move
                if (game.getPlayerId() != game.getTurn())
                    boardController.updateBoard(stringToArray(line),               // Move Origin
                                                stringToArray(readMessage()),     // Move Destination
                                                Integer.parseInt(readMessage()), // New Value
                                                stringToArray(readMessage())    // Remove this piece
                    );

                // This message will be gameover, connectionloss, movecomplete
                line = readMessage();

                // Break the loop immediatly
                if (line.equals("movecomplete")) {
                    game.switchTurn();
                    displayTurn();
                    // Break the loop immediatly
                } else if(line.equals("connectionloss") || line.equals("gameover"))
                    break;

            }

            // The server lost connection with the opponent
            if (line.equals("connectionloss"))
                boardController.updateGameStatus("Opponent Quit");
            // If this player is the same as the turn then they have won
            else if (Integer.toString(game.getPlayerId()).equals(readMessage()))
                boardController.updateGameStatus("You Win!!");
            else
                boardController.updateGameStatus("You Lose!!");
        } catch (IOException ioexception) {
            // Catch from readMessage because the read returned null
            boardController.updateGameStatus("Server Connection Lost");
        }
    }

    /** Read in a line from the server. If the line is null then the connection to the server has stopped. */
    public String readMessage() throws IOException {
        String line = "";
        line = serverIn.readLine();
        if (line == null)
            throw new IOException();

        return line;
    }

    /** Close the current socket in use to connect to the server. */
    public void close() {
        try {
            sock.close();
            System.out.println("Closed the socket");
        } catch (IOException ioexception) {
            ioexception.printStackTrace();
        }
    }

    /** Take a single int and convert it to a index in a 2 dimensional array. e.g 10 will be [1][1]*/
    private int[] stringToArray(String s) {
        String temp[] = s.split(",");
        return (new int[]{Integer.parseInt(temp[0]), Integer.parseInt(temp[1])
        });
    }

    /** Returns true if turn matches the player id. */
    public boolean myTurn() {
        return game.getTurn() == game.getPlayerId();
    }

    /** Sends the move in four packets.
     *
     * 1) The piece to move.
     * 2) Where to move it to.
     * 3) The value that it turns to on landing. 4) A piece to remove. The server will check to see if it has been set (!-1) .
     *
     * If an error has been found then the connection stream has been disconnected
     *
     * */
    public void sendMove(int origin[], int destination[], int destinationVal, int removePiece[]) {
        String line = "";
        serverOut.println((origin[0] + ",") + origin[1]);
        serverOut.println(destination[0] + "," + destination[1]);
        serverOut.println(destinationVal);
        serverOut.println(removePiece[0] + "," + removePiece[1]);
        if (serverOut.checkError())
            System.out.println("Problem trying to write to server");
    }

    /** When this side has ended it's move send a message to the server.
     *  Player may have had a move but is having another because the player can.
     */
    public void informSwitchTurn() {
        game.switchTurn();
        serverOut.println("movecomplete");
        displayTurn();
    }

    /** Display on the gui the status of the game. */
    public void displayTurn() {
        if (game.getTurn() == getPlayerId())
            boardController.updateGameStatus("Make your move..");
        else
            boardController.updateGameStatus("Waiting for opponents move..");
    }

    /** Send a blank message instead of "movecomlete". The server will listen for another move from this player. */
    public void anotherGo() {
        serverOut.println("");
    }

}