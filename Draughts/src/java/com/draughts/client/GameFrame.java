package com.draughts.client;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * A JFrame that holds different displays (JPanels).
 */
public class GameFrame extends JFrame {

    // Upper menu
    JMenuBar menuBar;
    // Hold the menu items
    JMenu options;
    // The menu items
    JMenuItem newGame;
    // Call back to start new game
    Client callBack;

    JPanel display;

    /*
     * Construct
     * ----------------------------------------------------------------------
     */
    public GameFrame(Client callBack, String title, JPanel display) {

        // Set the frame title
        super(title);

        this.display = display;

        this.callBack = callBack;

        // Set up the menu
        menuBar = new JMenuBar();
        newGame = new JMenuItem("New Game");
        newGame.addActionListener(new NewGameListener());
        options = new JMenu("Options");
        options.add(newGame);
        menuBar.add(options);
        setJMenuBar(menuBar);

        add(display);

        // Initialise the frame
        setSize(640, 640 + 80);
        setVisible(true);
        setLocationRelativeTo(null);
        setResizable(false);
        addWindowListener(new CloseListener());
    }

    // Listens for clicks in the menu
    public class NewGameListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent actionevent) {
            callBack.startNewGame();
        }
    }

    /** Whatever called this wants to display a message on the JPanel */
    public Displayable getDisplayable() { return (Displayable) display; }

    /** Listens for when the frame is closed by the user */
    public class CloseListener extends WindowAdapter {
        @Override
        public void windowClosing(WindowEvent windowevent) {
            callBack.close();
            System.exit(0);
        }

    }

}

