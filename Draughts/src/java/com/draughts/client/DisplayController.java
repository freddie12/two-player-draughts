package com.draughts.client;

import com.draughts.common.Updatable;

/**
 * Controls the Display of the game board. When the BoardController updates the Board it also updates this using the methods
 * implemented by Updatable.
 *
 * @see BoardController
 * @see Display
 * @see Updatable
 */
public class DisplayController implements Updatable {

    // Display JPanel instance
    private Display display;

    /*
     * Constructor
     * ----------------------------------------------------------------------
     */
    /** Receive a Display object to control. */
    public DisplayController(Display display) {
        this.display = display;
    }

    /*
     * Methods
     * ----------------------------------------------------------------------
     */
    /** Reset the display square that held the original selection. */
    public void resetMoveOrigin(int moveOrigin) {
        if (moveOrigin != -1) {
            display.setSquareColor("black", moveOrigin);
            update();
        }
    }

    /** Set the display move origin. */
    public void setMoveOrigin(int[] newOriginIndex) {
        resetMoveOrigin(display.getMoveOrigin());
        // Get the square number
        int i = arrToInt(newOriginIndex);
        display.setMoveOrigin(i);
        display.setSquareColor("green", i);
        update();
    }

    /** Update the displays JLabel with the current game status. */
    public void updateGameStatus(String status) {
        display.setMessage(status);
    }

    /** Update the display JLabel with a message. */
    public void displayMessage(String message) {
        display.setMessage(message);
        update();
    }

    /** Convert a 2D Array index to an int for use by the displays square/piece ArrayLists. */
    public int arrToInt(int[] index) {
        int i = index[0] * display.BOARD_SIZE + index[1];
        return i;
    }

    /** Move a Piece (BufferedImage) manipulating the Displays Piece ArrayList. */
    public boolean move(int[] destination) {

        // Origin and Destination
        int pieceOrigin = display.getMoveOrigin();
        int pieceDestination = arrToInt(destination);

        // Place origin piece into destination
        display.addPiece(pieceDestination, display.getPiece(pieceOrigin));

        // Update position X using square values
        int x = display.getSquare(pieceDestination).getPosX();
        display.getPiece(pieceDestination).setPosX(x);

        // Update position Y using square values
        int y = display.getSquare(pieceDestination).getPosY();
        display.getPiece(pieceDestination).setPosY(y);

        // Remove origin piece
        display.removePiece(pieceOrigin);

        // Reset the move origin background color
        display.getSquare(pieceOrigin).setColor("black");

        // Update the screen
        update();

        return true;

    }

    /** Turn the specific index to a king image. */
    public void makeKing(int[] index, int pieceValue) {
        if (pieceValue == -1)
            display.getPiece(arrToInt(index)).setBufferedImage(display.getImage(3));
        else
            display.getPiece(arrToInt(index)).setBufferedImage(display.getImage(2));
    }

    /** Remove a piece from the displays piece ArrayList. */
    public void removePiece(int[] index) { display.removePiece(arrToInt(index)); }

    /** Update the Display by repainting. */
    public void update() { display.repaint(); }

}
