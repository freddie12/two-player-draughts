package com.draughts.client;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * A simple JPanel with the welcome image for the background.
 */
public class MenuFrameDisplay extends JPanel implements Displayable {

    private BufferedImage image;
    private JLabel message;

    public MenuFrameDisplay() {
        try {
            image = ImageIO.read(new File("resources/images/Draughts Menu.png"));
        } catch (IOException ioexception) {
            ioexception.printStackTrace();
        }
        JPanel jpanel = new JPanel();
        message = new JLabel();
        setBackground(new Color(255, 255, 255));
        jpanel.add("South", message);
        add(jpanel);
    }

    public void setMessage(String s) {
        message.setText(s);
        repaint();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image, 0, 0, 640, 640, null);
    }

}