package com.draughts.client;

import com.draughts.common.Board;
import com.draughts.common.Updatable;

/**
 * Receive messages from the squares and manipulates the Board/Display under strict Draught game rules.
 */
public class BoardController implements Updatable {

    // The horizontal move direction
    private int hDirection;

    // The vertical move direction
    private int vDirection;

    // Board to control
    private Board board;

    // Need to callback to update the boards status
    GameController gameController;

    // Control the display
    private DisplayController displayController;

    // The Display panel
    private Display display;

    // The amount of times the user has tried to make a valid move. Will use this to give advise on what they are doing wrong
    private int moveTries = 0;

    /*
     * Constructor
     * ----------------------------------------------------------------------
     */

    /**
     * @param gameController In order to call back and get info such as the current turn.
     * @param board          Board to control.
     */
    public BoardController(GameController gameController, Board board) {
        this.board = board;
        this.gameController = gameController;
    }

    /*
     * Mutators
     * -----------------------------------------------------------------------
     */
    /** Set the players selection which is the origin of the players move. */
    public void setMoveOrigin(int[] index) {
        board.setMoveOrigin(index);
    }
    /**  Set the players chosen destination for the move. Index of the square in pieceMap in Board. */
    public void setMoveDestination(int[] moveDestination) {
        board.setMoveDestination(moveDestination);
    }
    /**  After a move has occurred then the variables used need to be reset. */
    public void resetMoveVars() {
        // Reset origin square status
        board.setSquareStatus(board.getMoveOrigin(), 0);
        board.resetMoveVariables();
        board.setRemovePiece(new int[]{-1, -1});
        moveTries = 0;
    }

    /** Sets the display to mirror the board. */
    public void setDisplay() {
        // Create a display and control for the screen display.
        display = new Display(this, board.getSquareMap(), board.getPieceMap());
        displayController = new DisplayController(display);
    }

    /** Sets the vertical move direction by comparing the origin index to the destination index. */
    public void setVerticalMoveDirection() {
        vDirection = (board.getMoveDestination()[0] > board.getMoveOrigin()[0]) ? -1 : 1;
    }

    /** Sets the horizontal move direction by comparing the origin index to the destination index. */
    public void setHorizontalMoveDirection() {
        hDirection = (board.getMoveDestination()[1] > board.getMoveOrigin()[1] ? -1 : 1);
    }

    /** Set the move destination value to -1 */
    public void resetMoveDestination() {
        board.setMoveDestination(new int[]{-1, -1});
    }

    /*
     * Getters
     * -----------------------------------------------------------------------
     */
    /**  Returns the current board object being controlled. */
    public Board getBoard() {
        return board;
    }
    /**  Returns the Display object for the current game session. */
    public Display getDisplay() {
        return display;
    }


    /*
     * Methods
     * -----------------------------------------------------------------------
     */
    /** Receives the index of the square clicked by the user. */
    public void squareListener(int[] index) {

        /*
         * If player selects a square and it is filled with his own piece (e.g. 1 or 2) then it is selectable
         * and clearly the player does not want to choose this square as the move destination
         */

        // Make sure it's the players turn and don't allow the player to make moves until the game has started
        if(gameController.myTurn() && gameController.getIsAlive()) {
            if (board.getSquareStatus(index) == gameController.getTurn() || board.getSquareStatus(index) == gameController.getTurn() + gameController.getTurn()) {

                // Update the board
                setMoveOrigin(index);

                // Update display if available
                if (displayController != null) {
                    // And then highlight the new selected square
                    displayController.setMoveOrigin(index);

                } // End if

            } // End if

            // Origin is set so move piece
            else if (board.moveOriginIsSet()) {

                // Set the move destination
                setMoveDestination(index);

                // Try to move
                if (move(index)) {

                    // The player might be able to take a piece however if the previous move did not take a piece then another move is not allowed
                    // If the first condition is false and second is false then the player can take and did take the previous move
                    if (!canTakeSpec(board.getMoveDestination()) || board.getRemovePiece()[0] == -1)
                        // Switch player
                        gameController.informSwitchTurn();
                    else
                        // This player can take another go
                        gameController.anotherGo();


                    // Reset the move variables
                    resetMoveVars();

                    // Set the can take flag if the next player can take
                    canTake();

                } // End if move()

            } // End Else

        } // End if myTurn()

    } // End method

    /**
     * Returns true if the move is successful
     * Move a piece into the destination specified from the origin saved in the square listener.
     * Checks that the move is valid and then updates the Board and (if available) the Display.
     *
     * @return
     */
    public boolean move(int[] destination) {

        // Check that the move is valid
        if (validMove()) {

            // If the player can take piece and the validation successful
            // Set the board values
            moveSelectedPieces();

            // Update display if available
            if (displayController != null)
                displayController.move(destination);

            // Check if piece can turn to king
            if ((destination[0] == 0 || destination[0] == board.BOARD_SIZE - 1) && !isKing(destination)) {

                // Make the piece a king (-1+-1 = -2)
                makeKing(destination, gameController.getTurn());

                // Update display if available
                if (displayController != null)
                    displayController.makeKing(destination, gameController.getTurn());

            }

            // Check if a piece should be removed
            if (board.getRemovePiece()[0] != -1) {
                // Remove the piece
                removePiece(board.getRemovePiece());
            }

            // Send the move after check if destination is king
            gameController.sendMove(board.getMoveOrigin(), board.getMoveDestination(), board.getSquareStatus(board.getMoveDestination()), board.getRemovePiece());

            return true;

        } else {

            // Not a valid move
            resetMoveDestination();
            moveTries++;

        }

        return false;

    }

    /**
     * Returns true if the move does not violate the move rules of Draughts
     * Takes in to account the origin and destination and returns true if the move is valid according to the status of pieces.
     *
     * @return
     */
    public boolean validMove() {

        // Increase the move tries
        moveTries++;

        // Get the destination
        int[] destination = board.getMoveDestination();

        // Set the players horizontal move direction
        setHorizontalMoveDirection();

        // Set the players vertical move direction
        setVerticalMoveDirection();

        // Move difference between the squares
        int moveDifference = (vDirection == 1 ? board.getMoveOrigin()[0] - destination[0] : destination[0] - board.getMoveOrigin()[0]);

        // Make sure this piece can move this direction
        if (correctDirection())

            // Make sure the destination is empty and the square is black
            if (board.empty(destination) && isBlackSquare(destination))

                // If the player wants to move 2 spaces then check that something will be taken in the path
                if (moveDifference == 2) {

                    // Hold the value of the square in between the origin and destination
                    int[] squareInQuestion = new int[]{destination[0] + vDirection, destination[1] + hDirection};

                    // Check the square in question
                    if (squareContainsOpposition(squareInQuestion)) {

                        // Save the removable piece
                        board.setRemovePiece(squareInQuestion);

                        // Square between contains opposite piece so proceed
                        return true;

                    }

                } else if (moveDifference == 1)

                    // Check if it can't take another piece and proceed
                    if (!canTake())
                        return true;


        // User is trying invalid moves
        if (moveTries > 2)
            // Notify the player that they have to take if they can
            displayMessage("You must take opponents piece if you can!");

        // Doesn't meet the conditions set for move
        return false;

    }

    /**  Set the move destination status with the move origin status. */
    public void moveSelectedPieces() {
        board.setSquareStatus(board.getMoveDestination(), board.getSquareStatus(board.getMoveOrigin()));
    }

    /**  Return true if the argument given has a status of 0. */
    public boolean isBlackSquare(int[] moveDestination) {
        if (board.getSquareMapSquareValue(moveDestination) == 1) return true;
        return false;
    }

    /** Set the Displays message JLabel to nothing. */
    public void clearMessage() {
        displayMessage("");
    }

    /** Set the Displays message JLabel to given argument. */
    public void displayMessage(String message) {
        if (displayController != null)
            displayController.displayMessage(message);
    }

    /** Set the Displays status JLabel to a specified status. */
    public void updateGameStatus(String status) {
        // If display is available
        if (displayController != null)
            displayController.updateGameStatus(status);
    }

    /**  Update the board quickly with specified arguments. Unlike move() method no checks for game rules are done here.
     *   Purely used for on of opponents move from the server.
     */
    public void updateBoard(int[] origin, int[] destination, int destinationValue, int[] removePiece) {
        board.setSquareStatus(origin, 0);
        board.setSquareStatus(destination, destinationValue);

        displayController.setMoveOrigin(origin);

        // Update display if available
        if (displayController != null)
            displayController.move(destination);


        if(destinationValue == gameController.getTurn()+gameController.getTurn())
            // Update display so that it's now a king image
            if (displayController != null)
                displayController.makeKing(destination, gameController.getTurn());
        // Remove a piece if needed
        if(removePiece[0] != -1)
            removePiece(removePiece);
    }



    /** Make the specified index a king value. */
    public void makeKing(int[] index, int pieceValue) { board.setSquareStatus(index, pieceValue + pieceValue); }
    /** Return true if the status of the index is of king value. */
    public boolean isKing(int[] index) {
        if (board.getSquareStatus(index) == -2 || board.getSquareStatus(index) == 2)
            return true;

        return false;
    }

    /** Removes a piece specified in the given argument from the board. */
    public void removePiece(int[] index) {
        board.setSquareStatus(index, 0);

        // Update display if available
        if (displayController != null)
            displayController.removePiece(index);
    }





    /** Returns true if there are no pieces of the opposition left depending on the current turn. */
    public boolean hasWon() {
        int opposingPieceId = gameController.getTurn() * -1;

        for (int[] y : board.getPieceMap())
            for (int x : y)
                if (x == opposingPieceId || x == opposingPieceId + opposingPieceId)
                    return false;

        return true;

    }

    /** Returns true if one of the current turns players pieces is close enough to remove from the board. */
    public boolean canTake() {

        // Keep the row count and square counted in the loop
        int rowCnt = 0;
        int squareCnt = 0;

        int turn = gameController.getTurn();

        // Go through all of this players pieces to check if squares in proximity are occupied by opponent. If yes then return true (can take)
        for (int[] row : board.getPieceMap()) {

            for (int square : row) {

                // Always check the forward squares
                if (square == turn || square == turn + turn)
                    if (canTakeSpecForward(new int[]{rowCnt, squareCnt}))
                        return true;

                // If the square is a king status (-2 or 2) then check backwards too
                if (square == turn + turn)
                    if (canTakeSpecBackward(new int[]{rowCnt, squareCnt}))
                        return true;

                // Move to next square
                squareCnt++;
            } // End inner for

            // Square reached max width
            squareCnt = 0;
            rowCnt++;

        } // End for

        // Non detected
        return false;

    } // End method

    /** Returns true if the given index piece is in a position to take a piece.
     *
     * Depending on the status of the index a check is made either forward or both forward and backwards.
     *
     * @param index
     * @return
     */
    public boolean canTakeSpec(int[] index) {

        int turn = gameController.getTurn();

        // Is a king
        int back = turn + turn;
        if (board.getSquareStatus(index) == turn + turn)
            // Check backwards
            if (canTakeSpecBackward(index))
                return true;

        // Check forwards
        if (canTakeSpecForward(index))
            return true;

        return false;

    }

    /** Returns true if the given index has a piece that is of opposite status in either diagonal squares to the front of the players move direction. */
    public boolean canTakeSpecForward(int[] index) {

        int turn = gameController.getTurn();

        int[] questionableSquare;

        // Check one diagonal way
        questionableSquare = new int[]{index[0] - turn, index[1] + (turn * -1)};

        // If the status of square is the opposition
        if (squareContainsOpposition(questionableSquare)) {

            // Save the square in line with square being questioned to check it's empty
            questionableSquare = new int[]{questionableSquare[0] - turn, questionableSquare[1] + (turn * -1)};

            // Check if it's empty
            if (board.empty(questionableSquare))
                // It's empty
                return true;

        }

        // Check other diagonal way
        questionableSquare = new int[]{index[0] + (turn * -1), index[1] + turn};

        // If the status of square is the opposition
        if (squareContainsOpposition(questionableSquare)) {

            // Save the square in line with square being questioned
            questionableSquare = new int[]{questionableSquare[0] + (turn * -1), questionableSquare[1] + turn};

            // Check if it's empty
            if (board.empty(questionableSquare))
                // It's empty
                return true;

        }

        return false;

    }

    /** Returns true if the given index has a piece that is of opposite status in either diagonal squares to the rear of the players move direction. */
    public boolean canTakeSpecBackward(int[] index) {

        int turn = gameController.getTurn();

        int[] questionableSquare;

        // Piece is a king so also check other direction
        questionableSquare = new int[]{index[0] + turn, index[1] + (turn * -1)};

        // If the status of square is the opposition
        if (squareContainsOpposition(questionableSquare)) {

            // Save the square behind the removeable piece
            questionableSquare = new int[]{questionableSquare[0] + turn, questionableSquare[1] + (turn * -1)};

            // Check if it's empty
            if (board.empty(questionableSquare))
                // It's empty
                return true;

        }

        // Piece is a king so also check other direction
        questionableSquare = new int[]{index[0] + turn, index[1] + turn};

        // If the status of square is the opposition
        if (squareContainsOpposition(questionableSquare)) {

            // Save the square in line with square being questioned
            questionableSquare = new int[]{questionableSquare[0] + turn, questionableSquare[1] + turn};

            // Check if it's empty
            if (board.empty(questionableSquare))
                // It's empty
                return true;
        }

        return false;
    }

    /** Returns true if the moving piece is moving in the correct direction for the rules of Draughts. */
    public boolean correctDirection() {

        // The turn
        int turn = gameController.getTurn();

        // Get the status of the moving piece
        int movingPieceVal = board.getSquareStatus(board.getMoveOrigin());

        // Check if king (-1 != -2)
        if (turn == movingPieceVal) {
            // Check that the players turn is moving in correct direction
            return (turn == vDirection);
        } // Can move in any direction

        return true;

    }

    /** Returns true if a given square does not contain this players piece. */
    public boolean squareContainsOpposition(int[] index) {

        // The status of square in question
        int questionableSquare;

        // The turn
        int turn = gameController.getTurn();

        try {
            questionableSquare = board.getSquareStatus(index);
            if (questionableSquare != 0 && questionableSquare != turn && questionableSquare != turn + turn)
                return true;
        } catch (ArrayIndexOutOfBoundsException aob) { /* The reference square must be at the edge of the board*/ }

        return false;

    } // End method

} // End Class
