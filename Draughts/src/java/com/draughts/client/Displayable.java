package com.draughts.client;

/**
 * Each JComponent that implements this will be able to receive and display messages
 */
public interface Displayable {
    void setMessage(String message);
}
