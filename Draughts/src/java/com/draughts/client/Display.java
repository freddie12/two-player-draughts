package com.draughts.client;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;

/**
 * Sets up a JPanel that can be added to a JFrame and manipulated by a display controller in order to play a game of Draughts.
 * Holds to ArrayLists of Square and Piece. The Squares have listeners and notify the BoardController when a particular Square is
 * clicked by the user. The BoardController then manipulates this through the DisplayController.
 */
public class Display extends JPanel implements Displayable {

    // Board Dimensions
    protected static final int SQUARE_SIZE  = 80;
    protected static final int BOARD_SIZE   = 8;

    // Ability to manipulate the pieces as we run the game
    private Map<Integer, Piece> pieces;

    // Hold the squares on the board and make them clickable
    private ArrayList<Square> squares;

    // Hold the bufferedImages so they are accessable from outside by index reference
    Map<Integer, BufferedImage> images = new HashMap<Integer, BufferedImage>();

    // Reference the board controller when user interacts with the gui
    private BoardController boardController;

    // Moving piece vars
    private int moveOrigin = -1;

    // Square map
    private int[][] squareMap;

    // Piece map
    private int[][] pieceMap;

    // Displays the status of the game
    JLabel gameStatus;

    // Displays a message to the user
    JLabel message;

    /*
     * Constructor
     * ----------------------------------------------------------------------
     */

    /**
     * Set up the Square/Piece objects and initialise the JPanel
     * @param boardController For the Square objects to call back to.
     * @param squareMap Map to create Square objects
     * @param pieceMap Map to create Piece objects
     */
    public Display(BoardController boardController, int[][] squareMap, int[][] pieceMap) {

        // Read in the images
        try {
            images.put(0, ImageIO.read(new File("resources/images/black.png")));
            images.put(1, ImageIO.read(new File("resources/images/red.png")));
            images.put(2, ImageIO.read(new File("resources/images/black-king.png")));
            images.put(3, ImageIO.read(new File("resources/images/red-king.png")));
        } catch(IOException ioe) { ioe.printStackTrace(); }

        // Save game control reference
        this.boardController = boardController;

        // Save the map
        this.squareMap = squareMap;
        this.pieceMap = pieceMap;

        // Make the size of this panel 64x40 wide
        setSize(BOARD_SIZE*SQUARE_SIZE, BOARD_SIZE*SQUARE_SIZE);

        // Hold the squares
        JPanel squarePanel = new JPanel();
        squarePanel.setLayout(new GridLayout(BOARD_SIZE, BOARD_SIZE, 0, 0));

        // Initialise the board and the pieces
        initBoard();

        // Add all the squares to the panel
        for(Square s : squares)
            squarePanel.add(s);

        // Add the panel
        add(BorderLayout.CENTER, squarePanel);

        // Create panel to hold the labels;
        JPanel messagePanel = new JPanel();
        message = new JLabel();
        messagePanel.add(BorderLayout.EAST, message);

        // Label with game status
        gameStatus = new JLabel();
        messagePanel.add(BorderLayout.WEST, gameStatus);

        // Add the panel
        add(BorderLayout.SOUTH, messagePanel);

    }

    /*
     * Setters
     * ----------------------------------------------------------------------
     */
    /** Set a particular square color receiving either "black", "white", "green". */
    public void setSquareColor(String color, int squareIndex) { squares.get(squareIndex).setColor(color); }
    /** Set the move origin so that the Square can be reset to it's original color after the move */
    public void setMoveOrigin(int moveOrigin) { this.moveOrigin = moveOrigin; }
    /** Add a new Piece to the ArrayList when moving. The Piece values come later from the Origin Piece. */
    public void addPiece(int index, Piece piece) { pieces.put(index, piece); }
    /** Remove a Piece from the ArrayList either when moving or when taking. */
    public void removePiece(int index) { pieces.remove(index); }
    /** Set a message for the user in the Display. */
    public void setMessage(String message) { this.message.setText(message); repaint(); }

    /*
     * Getters
     * -----------------------------------------------------------------------
     */
    /** Return the Piece object from the ArrayList using the specified index. */
    public Piece getPiece(int index) { return pieces.get(index); }
    /** /** Return the Square object from the ArrayList using the specified index. */
    public Square getSquare(int index) { return squares.get(index); }
    /** Return the move origin index. */
    public int getMoveOrigin() { return moveOrigin; }
    /** Return the image in the ArrayList of images at the specified index. */
    public BufferedImage getImage(int index) { return images.get(index); }

    /*
     * Methods
     * -----------------------------------------------------------------------
     */
    /** Set up the clickable square panels and save them in the square ArrayList. */
    private void initBoard() {

        // Init squares
        squares = new ArrayList<Square>();

        // Create the squares
        for(int y=0; y<BOARD_SIZE; y++)
            for (int x = 0; x < BOARD_SIZE; x++)
                if (squareMap[y][x] == 0)
                    squares.add(new Square("white", x * SQUARE_SIZE, y * SQUARE_SIZE, x, y));
                else
                    squares.add(new Square("black", x * SQUARE_SIZE, y * SQUARE_SIZE, x, y));

        // Init pieces
        pieces = new HashMap<Integer, Piece>();

        // Create the piece objects
        for (int y = 0; y < BOARD_SIZE; y++)
            for (int x = 0; x < BOARD_SIZE; x++) {
                if (pieceMap[y][x] == 1)
                    // Draw white piece
                    pieces.put(y * BOARD_SIZE + x, new Piece(x * SQUARE_SIZE, y * SQUARE_SIZE, images.get(0)));
                else if (pieceMap[y][x] == -1)
                    // Draw red piece
                    pieces.put(y * BOARD_SIZE + x, new Piece(x * SQUARE_SIZE, y * SQUARE_SIZE, images.get(1)));

            }

    }

    @Override
    public void paint(Graphics g) {

        super.paint(g);

        // Paint each piece onto the board
        for(Piece p : pieces.values())
            g.drawImage(p.getImage(), p.getPosX(), p.getPosY(), this);

    }

    /*
     * Inner Classes
     * -----------------------------------------------------------------------
     */
    /** Each square is a clickable reference to the board array. Clicking sends the squares index to the square listener in the BoardController. */
    public class Square extends JPanel implements MouseListener {

        private Color color;
        private Map<String, Color> colors;

        // Array positions
        private int[] arrPos = {0, 0};

        // JPanel positions
        private int posX;
        private int posY;

        /*
         * Constructor
         * ----------------------------------------------------------------------
         */
        public Square(String color, int posX, int posY, int arrPosX, int arrPosY) {

            arrPos[0] = arrPosY;
            arrPos[1] = arrPosX;
            initColors();
            this.color = colors.get(color);
            this.posX = posX;
            this.posY = posY+5;
            addMouseListener(this);
            setBackground(this.color);
            setPreferredSize(new Dimension(SQUARE_SIZE, SQUARE_SIZE));

        }

        /*
         * Methods
         * ----------------------------------------------------------------------
         */
        /** Initialise the colors that will be used in the game. */
        private void initColors() {
            colors = new HashMap<String, Color>();
            colors.put("white",  new Color(255, 255, 255));
            colors.put("black", new Color(0, 0, 0));
            colors.put("green", new Color(113, 173, 107));
        }

        /** Listens for mouse clicks on each square. */
        public void mouseClicked(MouseEvent e) {

            // Set the selected piece.
            boardController.squareListener(getArrPos());

        }
        // Unused mouse listeners
        public void mouseExited(MouseEvent e) {}
        public void mouseReleased(MouseEvent e) {}
        public void mouseEntered(MouseEvent e) {}
        public void mousePressed(MouseEvent e) {}

        /*
         * Setters
         * ----------------------------------------------------------------------
         */
        /** Sets the background color of this square */
        public void setColor(String colorName) {
            colorName = colorName.toLowerCase();
            setBackground(colors.get(colorName));
        }

        /*
         * Getters
         * ----------------------------------------------------------------------
         */
        /** Returns the current background color of this square. */
        public Color getColor() { return color; }
        /** Returns the horizontal position of the squares top left corner on the parent JPanel. */
        public int getPosX() { return posX; }
        /** Returns the vertical position of the squares top left corner on the parent JPanel. */
        public int getPosY() { return posY; }
        /** Returns the array index of the square. */
        public int[] getArrPos() { return arrPos; }

    }

    /** Inner Piece Class (Pun Not Intended) Create pieces to be displayed on the gui. */
    public class Piece {

        // Position of this images top left point
        private int posX;
        private int posY;

        // Hold the image
        private BufferedImage image;

        /*
         * Constructor
         * ----------------------------------------------------------------------
         */
        public Piece(int x, int y, BufferedImage image) {

            posX = x;
            posY = y+5;
            this.image = image;

        }

        /*
         * Mutators
         * -----------------------------------------------------------------------
         */
        /** Sets the horizontal upper left point of this image. */
        public void setPosX(int posX) { this.posX = posX; }
        /** Sets the vertical upper left point of this image. */
        public void setPosY(int posY) { this.posY = posY; }
        /** Sets this objects buffered image. */
        public void setBufferedImage(BufferedImage image) { this.image = image; }
        /*
         * Getters
         * -----------------------------------------------------------------------
         */
        /** Returns the horizontal upper left point of this image. */
        public int getPosX() { return posX; }
        /** Sets the vertical upper left point of this image. */
        public int getPosY() { return posY; }
        /** Returns this objects buffered image. */
        public BufferedImage getImage() { return image; }

    }

}
